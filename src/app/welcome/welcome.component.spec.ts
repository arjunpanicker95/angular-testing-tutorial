import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { WelcomeComponent } from './welcome.component';

import { UserService } from '../_services';

describe('WelcomeComponent (dependency test)', () => {

    let comp: WelcomeComponent;
    let fixture: ComponentFixture<WelcomeComponent>;
    let de: DebugElement;
    let userService: any;
    let el: HTMLElement;

    beforeEach(async(() => {

        const UserServiceStub: any = {
            isLoggedIn: true,
            user: { name: 'Tester' }
        };

        TestBed.configureTestingModule({
            declarations: [WelcomeComponent],
            providers: [
                { provide: UserService, useValue: UserServiceStub }
            ]
        });

        fixture = TestBed.createComponent(WelcomeComponent);
        comp = fixture.componentInstance;

        userService = TestBed.get(UserService);

        de = fixture.debugElement.query(By.css('.welcome'));
        el = de.nativeElement;
    }));

    it('should welcome user', () => {
        fixture.detectChanges();
        const content = el.textContent;
        expect(content).toContain('Welcome', 'Welcome ...');
        expect(content).toContain('Tester', 'expected name');
    });

    it('should welcome "Arjun"', () => {
        userService.user.name = 'Arjun';
        fixture.detectChanges();
        expect(el.textContent).toContain('Arjun');
    });

    it('should request login if not logged in', () => {
        userService.isLoggedIn = false;
        fixture.detectChanges();
        const contents = el.textContent;
        expect(contents).not.toContain('Welcome', 'not Welcome');
        expect(contents).toMatch(/login/i);
    });

});