import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../_services';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private userService: UserService, private route: Router) { }

  createForm(): void {
    this.loginForm = this.fb.group({
      username: ['', [ Validators.required ]],
      password: ['', [ Validators.required ]]
    });
  }

  ngOnInit() {
    this.createForm();
  }

  onSubmit(loginForm): void {
    console.log('Signed In: ' + loginForm.username);
    this.userService.loginUser(loginForm.username)
      .then((success) =>  {
        console.log(success);
        this.route.navigate(['/welcome']);
      })
      .catch((fail) => {
        console.log(fail);
      });
  }

  get username() { return this.loginForm.get('username'); }
  get password() { return this.loginForm.get('password'); }

}
