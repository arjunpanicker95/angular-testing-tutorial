export interface QuoteModel {
  id: number;
  author: string;
  quote: string;
}