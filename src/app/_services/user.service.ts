import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  isLoggedIn = false;

  user: any = {
    name: '',
  };

  constructor() { }

  loginUser(username: string): Promise<boolean> {
    return new Promise ((resolve, reject) => {
      if (username !== null && username !== '' && username !== undefined) {
        console.log('If: ' + username);
        this.user.name = username;
        this.isLoggedIn = true;
        resolve(this.isLoggedIn);
      } else {
        console.log('else: ' + username);
        reject(this.isLoggedIn);
      }
    });
  }

}
