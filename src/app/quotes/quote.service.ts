import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/toPromise';

import { QuoteModel } from '../_models/quote.model';

const api = '/quotes';

@Injectable()
export class QuoteService {

  quote: QuoteModel;
  
  constructor(private http: HttpClient) { }

  getQuote(): Promise<QuoteModel> {
    return this.http.get<QuoteModel>('/quotes')
      .toPromise()
      .then((data) => data)
      .catch(() => Promise.reject(null));
  }

}
