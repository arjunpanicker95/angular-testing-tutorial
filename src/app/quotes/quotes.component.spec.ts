import { TestBed, ComponentFixture, async, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
 
import { QuoteService } from './quote.service';
import { QuotesComponent } from './quotes.component';

import { QuoteModel } from '../_models/quote.model';

describe('QuotesComponent (async test)', () => {
  
  let comp: QuotesComponent;
  let fixture: ComponentFixture<QuotesComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  let quoteService, spy: any;
  const testQuote: QuoteModel = {
    id: 1,
    author: 'Test',
    quote: 'This is a test quote'
  };
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ QuotesComponent ],
      providers: [ QuoteService, HttpTestingController ]
    });
  
    fixture = TestBed.createComponent(QuotesComponent);
    comp = fixture.componentInstance;
    
//    QuoteService actually injected into the component
    quoteService = fixture.debugElement.injector.get(QuoteService);
    
//    Setup spy on the 'getQuote' method
    spy = spyOn(quoteService, 'getQuote')
          .and.returnValue(Promise.resolve(testQuote));
    
//    Get the quote element by a CSS selector
    de = fixture.debugElement.query(By.css('.quote-style'));
    el = de.nativeElement;
    
  });
  
  it('should not display any quote before OnInit', () => {
    expect(el.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getQuote not yet called');
  });
  
  it('should still not show quote after component initialized', () => {
//    quoteService. = null;
    fixture.detectChanges();
    expect(el.textContent).toBe('', 'no quote yet');
    expect(spy.calls.any()).toBe(true, 'getQuote called');
  });
  
  it('should show quote after getQuote promise (async)', async(() => {
    fixture.detectChanges();
    
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(el.textContent).toBe(testQuote.quote);
    });
  }));
  
  it('should show quote after getQuote promise (fakeAsync)', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    fixture.detectChanges();
    expect(el.textContent).toBe(testQuote.quote);
  }));
  
});