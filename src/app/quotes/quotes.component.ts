import { Component, OnInit } from '@angular/core';

import { QuoteModel } from '../_models/quote.model';

import { QuoteService } from './quote.service';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  
  quote: QuoteModel;
  displayQuote: string;

  constructor(private quoteService: QuoteService) {}

  ngOnInit(): void {
    this.quoteService.getQuote()
        .then(quote => {
            this.quote = quote;
            this.displayQuote = this.quote.quote;
        })
        .catch(() => console.log('Error fetching data from the server'));
  }

}
